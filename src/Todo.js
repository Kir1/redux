import {connect} from 'react-redux';
import React from 'react';

class Todo extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.remove = this.remove.bind(this);
    }

    handleChange(event) {
        this.props.change(this.props.index, event.target.value);
    }

    remove(event) {
        this.props.remove(this.props.index);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.todo.text == '12' &&  this.props.todo.text == '1') {
            console.log('example no render');
            return false;
        }
        return true;
    }

    render() {
        console.log(this.props ,'this.props.todo');

        return (
            <div className="">
                <input type="text" placeholder="Текст новой записи" value={this.props.todo.text}
                       onChange={this.handleChange}/>
                <input type="submit" onClick={this.remove} value="Удалить"/>
            </div>
        );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    change: (index, text) => dispatch({type: 'CHANGE', index, text}),
    remove: (index) => dispatch({type: 'REMOVE', index})
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Todo)