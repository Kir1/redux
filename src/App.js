import './App.css';
import Todos from './Todos';
import {Provider} from 'react-redux';
import {createStore, combineReducers} from 'redux';
import React from 'react';

const initialState = {
    todos: [
        {text: '1'},
        {text: '2'},
        {text: '3'}
    ],
    textNewTodo: ''
};

const game = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD':
            var todos = [...state.todos];
            todos.push({text: state.textNewTodo});
            return {...state, todos};

        case 'CHANGE-FORM':
            return {...state, textNewTodo: action.text};

        case 'CHANGE':
            var todos = [...state.todos];
            todos[action.index] = {...todos[action.index], text: action.text}
            return {...state, todos};

        case 'REMOVE':
            var todos = [...state.todos];
            todos.splice(action.index, 1);
            return {...state, todos};

        default:
            return state;
    }
}

const rootReducer = combineReducers({
    game
});

const store = createStore(rootReducer);

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Todos />
            </Provider>
        );
    }
}

export default App;
