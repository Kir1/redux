import Todo from './Todo';
import {connect} from 'react-redux';
import React from 'react';

class Todos extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.props.changeForm(event.target.value);
    }

    handleSubmit() {
        this.props.pushTodo();
    }

    render() {
        return (
            <div className="">
                <input type="text" placeholder="Текст новой записи" value={this.props.textNewTodo}
                       onChange={this.handleChange}/>
                <input type="submit" onClick={this.handleSubmit} value="Добавить запись"/>
                {this.props.todos.map((todo, index) => (
                    <Todo key={index} todo={todo} index={index}/>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    todos: state.game.todos,
    textForm: state.game.textNewTodo
})

const mapDispatchToProps = dispatch => ({
    pushTodo: () => dispatch({type: 'ADD'}),
    changeForm: (text) => dispatch({type: 'CHANGE-FORM', text})
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Todos)